cd ../
# npm install -g http-server
CALL npm install ./
# npm run build -- --prod
# npm run build:ssr -- --prod
CALL ng build --aot --prod
CALL http-server ./dist/webapp

pause
