import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';
import { TokenService } from '../../services/token.service';

import { RegisterUserModel, LoginUserModel, Client } from '../../services/webapiclient.service';
import { TemplateJsFunctionLoaderService } from '../../services/template.jsfunctionloader.service';
import { TranslationService, Language } from 'angular-l10n';
import { ContextService } from '../../services/context.service';

//import * as $ from "jquery";
@Component({
    selector: 'login',
    templateUrl: './login.component.html'
})

export class LoginComponent {
    @Language() lang: string;

    public loginModel: LoginUserModel;
    public registerModel: RegisterUserModel;
    private router: Router;
    private webClient: Client;
    private templatejsfunction: TemplateJsFunctionLoaderService;
    private contextService: ContextService;

  constructor(public translationService: TranslationService, @Inject(Router) router: Router, private http: HttpClient, @Inject('BASE_URL') baseUrl: string, @Inject(Client) webClient: Client, @Inject(TemplateJsFunctionLoaderService) templatejsfunction: TemplateJsFunctionLoaderService, @Inject(ContextService) contextService: ContextService, private tokenManager: TokenService) {
        this.router = router;
        this.loginModel = new LoginUserModel();
        this.registerModel = new RegisterUserModel();
        this.webClient = webClient;
        this.loginModel.rememberMe = true;
        this.registerModel.gender = "Male";
        this.templatejsfunction = templatejsfunction;
        this.contextService = contextService;
    }

    arrayOne(n: number): any[] {
        return Array(n);
    }

    subDateOne(): any[] {
        var all = new Array();
        var today = new Date();

        var todayDate = today.getFullYear();
        var lastDate = 1901;

        all.push(0);
        while (todayDate < lastDate)
        {
            all.push(todayDate);
            todayDate--;
        }
        return all;
    }

    createScript(scriptlink: string) {
        var sNew = document.createElement("script");
        sNew.async = false;
        sNew.src = scriptlink;
        sNew.type = "text/javascript";
        var s0 = document.getElementsByTagName('script')[0];
        if (!(typeof s0 === 'undefined' || s0.parentNode == null)) {
            s0.parentNode.insertBefore(sNew, s0);
        }
    }

    ngOnInit() {
        this.templatejsfunction.createScript("/assets/ThemeRedstar/assets/jquery.min.js");
        this.templatejsfunction.createScript("/assets/ThemeRedstar/assets/login.js");
        this.templatejsfunction.createScript("/assets/ThemeRedstar/assets/pages.js");
    }

    login() {
        this.tokenManager.clearToken();
        this.webClient.apiAccountLoginPost(this.loginModel).subscribe(
            res => {
                this.tokenManager.setToken(res.token);
                this.contextService.resetProfile();
                this.router.navigate(['/index']);
            },
            err => {
            });
    }

    register() {
        //check si la date de naissance reunis tous les bonnes conditions
        this.webClient.apiAccountRegisterPost(this.registerModel).subscribe(
            res => {
                this.router.navigate(['/login']);
            },
            err => {
                console.log(err);
            });
    }
}
