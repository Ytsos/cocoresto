import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BaseComponent } from '../../../helpers/BaseComponent';
import { TranslationService, Language } from 'angular-l10n';
import { CutUrlService } from '../../../services/cuturl.service';
import * as Chart from 'chart.js'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'homepanel',
  templateUrl: './homepanel.component.html'
})
export class HomePanelComponent extends BaseComponent {
  @Language() lang: string;

  view: any[] = [700, 400];

  canvas: any;
  ctx: any;

  ngAfterViewInit() {
    this.canvas = document.getElementById('myChart');
    this.ctx = this.canvas.getContext('2d');
    let myChart = new Chart(this.ctx, {
      type: 'pie',
      data: {
        labels: ["New", "In Progress", "On Hold"],
        datasets: [{
          label: '# of Votes',
          data: [1, 2, 3],
          backgroundColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        responsive: false,
        display: true
      }
    });
  }

  // line, area
  autoScale = true;

  constructor(private toastr: ToastrService, public translationService: TranslationService, public router: Router, public route: ActivatedRoute, public cutUrlService: CutUrlService) {
    super(translationService, router, route);
  }

    ngOnInit() {
    }

  showSuccess() {
    this.toastr.success('Hello world!', 'Toastr fun!', { positionClass: "toast-bottom-center", timeOut: 9999999999999 });
    this.toastr.warning('Hello world!', 'Toastr fun!', { positionClass: "toast-bottom-center", timeOut: 9999999999999 });
    this.toastr.error('Hello world!', 'Toastr fun!', { positionClass: "toast-bottom-center", timeOut: 9999999999999 });
    this.toastr.info('Hello world!', 'Toastr fun!', { positionClass: "toast-bottom-center", timeOut: 9999999999999 });
  }
}
