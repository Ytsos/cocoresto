import { Observable } from "rxjs/Observable";
import { Inject, Component, Injectable } from "@angular/core";
import { CookieService } from 'ngx-cookie';

@Injectable({ providedIn: 'root' })
export class TokenService {
    private readonly keyToken: string = "token";
    private readonly decryptEncryptKeyToken: string = "token";

    constructor(private cookieService: CookieService) {
    }

    getToken(): string {
        let token = this.cookieService.get(this.keyToken);
        if (token === null) {
            return "";
        }
        return token;
    }

    getTokenObs(): Observable<string> {
        let token = this.cookieService.get(this.keyToken);
        if (token === null) {
            return Observable.of("");
        }
        return Observable.of(token);
    }


    setToken(token: string) {
        this.cookieService.put(this.keyToken, token);
    }

    clearSpecificTokenByName(tokenName: string) {
        this.cookieService.remove(tokenName);
    }

    clearSpecificToken() {
        this.cookieService.remove(this.keyToken);
    }

    clearToken() {
        this.cookieService.removeAll();
    }
}
