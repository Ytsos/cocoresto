import { TokenService } from "./token.service";
import { HttpHeaders } from '@angular/common/http';

export class BaseClient {
    constructor(public token: TokenService) {
    }

  transformOptions(options: any) {
    return this.token.getTokenObs().map(res => {
      if (res != null) {
        var headers_object = new HttpHeaders({ 'Authorization': 'Bearer ' + res });
        options.headers = headers_object;
      }
      //options.headers.append("Authorization", "Bearer " + res);
      return options;
    }).toPromise();
  }
}
