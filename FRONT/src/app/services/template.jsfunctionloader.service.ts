import { Observable } from "rxjs/Observable";
import { Inject, Component, Injectable } from "@angular/core";

@Injectable({ providedIn: 'root' })
export class TemplateJsFunctionLoaderService {

    constructor() {
    }

    createScript(scriptlink: string) {
        var sNew = document.createElement("script");
        sNew.async = false;
        sNew.src = scriptlink;
        sNew.type = "text/javascript";
        var s0 = document.getElementsByTagName('script')[0];
        if (!(typeof s0 === 'undefined' || s0.parentNode == null)) {
            s0.parentNode.insertBefore(sNew, s0);
        }
    }
}
