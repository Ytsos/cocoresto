import { Injectable, Inject } from "@angular/core";
import { Router } from "@angular/router";
import { Client, ProfileInfoDto } from "./webapiclient.service";

@Injectable({ providedIn: 'root' })
export class ContextService {
  public profile: ProfileInfoDto;

  constructor(private router: Router, private webClient: Client) {
  }

  load(): void {
  }

  loadProfile(): void {
    this.webClient.apiAccountGetprofileinfoGet().subscribe(
      res => {
        this.profile = res;
      },
      err => {
        this.profile = null;
      });
  }

  resetProfile(): void {
    this.profile = null;
  }
}
