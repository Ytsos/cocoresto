import { RequestOptions } from "@angular/http";
import { BaseComponent } from "../helpers/BaseComponent";
import { Observable } from "rxjs/Observable";
import { Router, ActivatedRoute } from "@angular/router";
import { TokenService } from "./token.service";
import { Inject, Injectable } from "@angular/core";
import { validateConfig } from "@angular/router/src/config";
import { LoginUserModel, Client } from "./webapiclient.service";
import { TranslationService } from "angular-l10n";

@Injectable({ providedIn: 'root' })
export class AuthenticationService extends BaseComponent {
    private readonly keyToken: string = "token";
    private readonly decryptEncryptKeyToken: string = "token";
    private apiClient: Client;
    private tokenManager: TokenService;

    constructor(@Inject(TranslationService) translationService: TranslationService, @Inject(Router) router: Router, @Inject(ActivatedRoute) route: ActivatedRoute, @Inject(Client) apiClient: Client, @Inject(TokenService) tokenManager: TokenService) {
        super(translationService, router, route);
        this.apiClient = apiClient;
        this.tokenManager = tokenManager;
    }

    login(loginForm: LoginUserModel): Observable<boolean> {
        return this.apiClient.apiAccountLoginPost(loginForm).map(res => { this.tokenManager.setToken(res.token); return true; })
            .catch(err => Observable.of(false));
    }

    isAuthenticated(): Observable<boolean> {
        return this.apiClient.apiAccountIsloggedGet().map(res => { return true; })
            .catch(err => Observable.of(false));
    }

    logout(): void {
        this.apiClient.apiAccountLogoutDelete().subscribe(res => { }, err => { });
        this.tokenManager.clearSpecificToken();
    }
}
