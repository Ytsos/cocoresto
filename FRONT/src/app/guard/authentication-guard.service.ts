import { Injectable, Inject } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({ providedIn: 'root' })
export class IsAuthenticatedGuard implements CanActivate {
    private authService: AuthenticationService;
    private router: Router;

    constructor( @Inject(Router) router: Router, @Inject(AuthenticationService) authService: AuthenticationService) {
        this.authService = authService;
        this.router = router;
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.authService.isAuthenticated().map(res => {
            if (res == true) {
                return true;
            }
            else {
                this.router.navigate(['/panel/login']);
                return false;
            }
        })
        .catch(
        err => {
            this.router.navigate(['/panel/login']);
            return Observable.of(false);
        });
    }

    //canDeactivate(component: LoginComponent, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    //    return this.authService.isAuthenticated().map(res => {
    //        if (res == true) {
    //            this.router.navigate(['/panel']);
    //            return false;
    //        }
    //        return true;
    //    })
    //        .catch(
    //        err => {
    //            return Observable.of(true);
    //        });
    //}
}

@Injectable({ providedIn: 'root' })
export class IsNotAuthenticatedGuard implements CanActivate {
    private authService: AuthenticationService;
    private router: Router;

    constructor( @Inject(Router) router: Router, @Inject(AuthenticationService) authService: AuthenticationService) {
        this.authService = authService;
        this.router = router;
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.authService.isAuthenticated().map(res => {
            if (res == true) {
                this.router.navigate(['/panel/index']);
                return false;
            }
            return true;
        })
        .catch(
        err => {
            return Observable.of(true);
        });
    }
}
