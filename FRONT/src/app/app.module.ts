import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { IsAuthenticatedGuard, IsNotAuthenticatedGuard } from './guard/authentication-guard.service';
import { HomeComponent } from './components/home.component';
import { LoginComponent } from './components/account/login.component';

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    children: [
      { path: '', component: HomeComponent }
    ]
  },
  {
    path: '/login',
    component: LoginComponent,
    canActivate: [IsNotAuthenticatedGuard]
  }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    RouterModule.forRoot(AppRoutes),
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }