package com.cocoresto.query.Controller;

import com.cocoresto.query.Model.SearchFilter;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.cocoresto.query.Model.Constants.HEADER_STRING;
import static com.cocoresto.query.Model.Constants.TOKEN_PREFIX;

@RestController
public class QueryController {

    @Autowired
    private SparkSession _spark;
    private Map<String, Dataset<Row>> _files;

    @Value("${spark.path.businessFile}")
    private String business_path;
    @Value("${spark.path.photoFile}")
    private String photo_path;
    @Value("${spark.authTokenCheck}")
    private String auth_token;

    public QueryController() {
        _files = new HashMap<>();
    }

    private void loadAllFiles() {
        if (_files.isEmpty()) {
            _files.put("business", _spark.read().json(business_path));
            _files.put("photo", _spark.read().json(photo_path));
            //_files.put("review", _spark.read().json("d:/EPITECH/CLOUDCOMPUTING/data/yelp_dataset/yelp_academic_dataset_review.json"));
        }
    }
    private boolean checkAuth(HttpServletRequest req) throws IOException {
        String header = req.getHeader(HEADER_STRING);
        String authToken = null;

        if (header != null && header.startsWith(TOKEN_PREFIX)) {
            authToken = header.replace(TOKEN_PREFIX,"");

            URL url = new URL(auth_token);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty(HEADER_STRING, TOKEN_PREFIX + authToken);
            int responseCode = con.getResponseCode();
            if (responseCode == 200) {
                return true;
            }
        }
        return false;
    }


    private String checkEmptyString(String original, String filter){
        if(!original.isEmpty()){
            return "AND " + filter;
        }
        return filter;
    }

    private String returnFilter(SearchFilter filter) {
        String filterString = "";

        if (filter.IsOpen) {
            filterString += checkEmptyString(filterString, "is_open = 1");
        }
        if (filter.Delivery) {
            filterString += checkEmptyString(filterString, "attributes.RestaurantsDelivery = 'True'");
        }
        if (filter.DogsAllow) {
            filterString += checkEmptyString(filterString, "attributes.DogsAllowed = 'True'");
        }
        if (filter.FullBar) {
            filterString += checkEmptyString(filterString, "attributes.Alcohol = 'full_bar'");
        }
        if (filter.Open24Hours) {
            filterString += checkEmptyString(filterString, "attributes.Open24Hours = 'True'");
        }
        if (filter.OutdoorSeating) {
            filterString += checkEmptyString(filterString, "attributes.OutdoorSeating = 'True'");
        }
        if (filter.TakeOut) {
            filterString += checkEmptyString(filterString, "attributes.RestaurantsTakeOut = 'True'");
        }
        if (filter.TakesReservations) {
            filterString += checkEmptyString(filterString, "attributes.RestaurantsReservations = 'True'");
        }
        if (filter.PriceRange > 0 && filter.PriceRange < 5) {
            filterString += checkEmptyString(filterString, "attributes.RestaurantsPriceRange2 = '" + filter.PriceRange + "'");
        }
        return filterString;
    }

    @RequestMapping(value="/restosearch/", method = RequestMethod.POST, params = {"type"})
    public ResponseEntity<?> getRestoInfoSearch(@RequestBody SearchFilter filter, @RequestParam(value = "type") String type) {
        this.loadAllFiles();
        try {
            Dataset<Row> businessJsonFile = _files.get("business");//_spark.sparkContext().listFiles());
            String filterString = this.returnFilter(filter);
            JavaRDD<String> res = (filter.Limit == 0) ? businessJsonFile.select("address", "attributes",
                    "business_id", "categories", "city", "hours", "is_open", "latitude", "longitude",
                    "name", "neighborhood", "postal_code", "review_count", "stars", "state")
                    .filter("categories like '%"+ type +"%'")
                    .filter(filterString)
                    .toJSON()
                    .toJavaRDD()
                    :
                    businessJsonFile.select("address", "attributes",
                            "business_id", "categories", "city", "hours", "is_open", "latitude", "longitude",
                            "name", "neighborhood", "postal_code", "review_count", "stars", "state")
                            .filter("categories like '%"+ type +"%'")
                            .filter(filterString)
                            .limit(filter.Limit)
                            .toJSON()
                            .toJavaRDD();

            List<String> resLitString = res.collect();
            if (resLitString.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return ResponseEntity.ok(resLitString);
        }
        catch (Exception ignored){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
