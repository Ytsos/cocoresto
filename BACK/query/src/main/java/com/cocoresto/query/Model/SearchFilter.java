package com.cocoresto.query.Model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SearchFilter {
    @JsonProperty("nbQuerySkip")
    public int NbQuerySkip;

    @JsonProperty("limit")
    public int Limit;

    @JsonProperty("priceRange")
    public int PriceRange;

    @JsonProperty("isOpen")
    public boolean IsOpen;

    @JsonProperty("takesReservations")
    public boolean TakesReservations;

    @JsonProperty("delivery")
    public boolean Delivery;

    @JsonProperty("dogsAllow")
    public boolean DogsAllow;

    @JsonProperty("takeOut")
    public boolean TakeOut;

    @JsonProperty("fullBar")
    public boolean FullBar;

    @JsonProperty("open24Hours")
    public boolean Open24Hours;

    @JsonProperty("outdoorSeating")
    public boolean OutdoorSeating;
}