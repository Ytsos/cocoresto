package com.cocoresto.information.Controller;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.cocoresto.information.Model.Constants.HEADER_STRING;
import static com.cocoresto.information.Model.Constants.TOKEN_PREFIX;

@RestController
public class InfoController {

    @Autowired
    private SparkSession _spark;
    private Map<String, Dataset<Row>> _files;

    @Value("${spark.path.businessFile}")
    private String business_path;
    @Value("${spark.path.photoFile}")
    private String photo_path;
    @Value("${spark.authTokenCheck}")
    private String auth_token;

    public InfoController() {
        _files = new HashMap<>();
    }

    private void loadAllFiles() {
        if (_files.isEmpty()) {
            _files.put("business", _spark.read().json(business_path));
            _files.put("photo", _spark.read().json(photo_path));
            //_files.put("review", _spark.read().json("d:/EPITECH/CLOUDCOMPUTING/data/yelp_dataset/yelp_academic_dataset_review.json"));
        }
    }
    private boolean checkAuth(HttpServletRequest req) throws IOException {
        String header = req.getHeader(HEADER_STRING);
        String authToken = null;

        if (header != null && header.startsWith(TOKEN_PREFIX)) {
            authToken = header.replace(TOKEN_PREFIX,"");

            URL url = new URL(auth_token);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty(HEADER_STRING, TOKEN_PREFIX + authToken);
            int responseCode = con.getResponseCode();
            if (responseCode == 200) {
                return true;
            }
        }
        return false;
    }

    @RequestMapping(value="/restoinfo/{business_id}", method = RequestMethod.GET)
    public ResponseEntity<?> getRestoInfo(@PathVariable("business_id") String business_id, HttpServletRequest req) {
        this.loadAllFiles();
//        try {
//            if (!this.checkAuth(req))
//                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
//        }
//        catch (Exception e) {
//            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
//        }
        try {
            Dataset<Row> businessJsonFile = _files.get("business");//_spark.sparkContext().listFiles());
            JavaRDD<String> res = businessJsonFile.select("address", "attributes",
                    "business_id", "categories", "city", "hours", "is_open", "latitude", "longitude",
                    "name", "neighborhood", "postal_code", "review_count", "stars", "state")
                    .filter("business_id = '" + business_id + "'")
                    .toJSON()
                    .toJavaRDD();

            String resString = res.first();
            if (resString.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return ResponseEntity.ok(resString);
        }
        catch (Exception ignored){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value="/restoinfophoto/{business_id}", method = RequestMethod.GET)
    public ResponseEntity<?> getRestoInfoPhoto(@PathVariable("business_id") String business_id) {
        this.loadAllFiles();
        try {
            Dataset<Row> businessJsonFile = _files.get("photo");//_spark.sparkContext().listFiles());
            JavaRDD<String> res = businessJsonFile.select("photo_id", "business_id",
                    "caption", "label")
                    .filter("business_id = '" + business_id + "'")
                    .toJSON()
                    .toJavaRDD();

            List<String> resLitString = res.collect();
            if (resLitString.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return ResponseEntity.ok(resLitString);
        }
        catch (Exception ignored){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value="/restoinfofirstphoto/{business_id}", method = RequestMethod.GET)
    public ResponseEntity<?> getRestoInfoFirstPhoto(@PathVariable("business_id") String business_id) {
        this.loadAllFiles();
        try {
            Dataset<Row> photoJsonFile = _files.get("photo");//_spark.sparkContext().listFiles());
            JavaRDD<String> res = photoJsonFile.select("photo_id", "business_id",
                    "caption", "label")
                    .filter("business_id = '" + business_id + "'")
                    .toJSON()
                    .toJavaRDD();

            String resString = res.first();
            if (resString.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return ResponseEntity.ok(resString);
        }
        catch (Exception ignored){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
