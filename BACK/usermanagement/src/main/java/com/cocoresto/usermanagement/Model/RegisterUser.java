package com.cocoresto.usermanagement.Model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class RegisterUser implements Serializable {
    @NotNull
    @JsonProperty("username")
    public String username;

    @NotNull
    @JsonProperty("password")
    public String password;

    @NotNull
    @JsonProperty("firstname")
    public String firstname;

    @NotNull
    @JsonProperty("lastname")
    public String lastname;

    @JsonProperty("zipCode")
    public String zipCode;

    @JsonProperty("birthDay")
    public Integer birthDay;

    @JsonProperty("birthMonth")
    public Integer birthMonth;

    @JsonProperty("birthYear")
    public Integer birthYear;
}
