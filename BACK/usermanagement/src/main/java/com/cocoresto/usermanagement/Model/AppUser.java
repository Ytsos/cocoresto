package com.cocoresto.usermanagement.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "user")
public class AppUser implements Serializable {
    @Id
    @JsonProperty("id")
    private ObjectId id;
    @JsonProperty("username")
    private String username;
    @JsonProperty("password")
    private String password;
    @JsonProperty("firstname")
    private String firstname;
    @JsonProperty("lastname")
    private String lastname;
    @JsonProperty("role")
    private String role;
    @JsonProperty("zipCode")
    private String zipCode;
    @JsonProperty("birthDay")
    private Integer birthDay;
    @JsonProperty("birthMonth")
    private Integer birthMonth;
    @JsonProperty("birthYear")
    private Integer birthYear;

    public AppUser() {
        this.id = new ObjectId();
    }

    public AppUser(ObjectId id, String username, String password,
                   String role, String zipCode,
                   String firstname, String lastname,
                   Integer birthDay, Integer birthMonth, Integer birthYear) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.role = role;
        this.zipCode = zipCode;
        this.birthDay = birthDay;
        this.birthMonth = birthMonth;
        this.birthYear = birthYear;
    }

    public org.bson.Document getDocument() {
        org.bson.Document newDoc = new org.bson.Document();

        newDoc.append("id", this.id);
        newDoc.append("username", this.username);
        newDoc.append("password", this.password);
        newDoc.append("firstname", this.firstname);
        newDoc.append("lastname", this.lastname);
        newDoc.append("role", this.role);
        newDoc.append("zipCode", this.zipCode);
        newDoc.append("birthDay", this.birthDay);
        newDoc.append("birthMonth", this.birthMonth);
        newDoc.append("birthYear", this.birthYear);

        return newDoc;
    }

    public String getId() {
        return this.id.toHexString();
    }

    public ObjectId getOriginalId() {
        return this.id;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public String getRole() {
        return this.role;
    }

    public String getZipCode() {
        return this.zipCode;
    }

    public Integer getBirthDay() {
        return this.birthDay;
    }

    public Integer getBirthMonth() {
        return this.birthMonth;
    }

    public Integer getBirthYear() {
        return this.birthYear;
    }


    public void setId(String id) {
        this.id = new ObjectId(id);
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public void setBirthDay(int birthDay) {
        this.birthDay = birthDay;
    }

    public void setBirthMonth(int birthMonth) {
        this.birthMonth = birthMonth;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }
}
