package com.cocoresto.usermanagement.Service;

import com.cocoresto.usermanagement.Configuration.MongoConfiguration;
import com.cocoresto.usermanagement.Model.AppUser;
import org.bouncycastle.crypto.generators.BCrypt;
import org.bson.BsonDocument;
import org.bson.Document;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Arrays;
import java.util.List;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;
import static org.springframework.data.mongodb.core.query.Update.update;

public class MongoDbConnection implements UserDetailsService {
    private MongoOperations _mongoOperations;
    private String _collectionName;

    public MongoDbConnection(String collection) {
        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(MongoConfiguration.class);
        this._mongoOperations = (MongoOperations) ctx.getBean("mongoTemplate");
        _collectionName = collection;
    }

    public AppUser findWithUsername(String username) {
        Query query = query(where("username").is(username));
        AppUser findUserWithName = _mongoOperations.findOne(query, AppUser.class);
        return findUserWithName;
    }

    public boolean saveOrUpdate(AppUser user) {
        AppUser findUser = _mongoOperations.findById(user.getOriginalId(), AppUser.class);

        Query queryUsername = query(where("username").is(user.getUsername()));
        AppUser findUserWithName = _mongoOperations.findOne(queryUsername, AppUser.class);

        Query query;
        if (findUser != null) {
            user.setId(findUser.getId());
            query = query(where("_id").is(user.getId()));
        }
        else if (findUserWithName != null) {
            user.setId(findUserWithName.getId());
            query = query(where("username").is(user.getUsername()));
        }
        else {
            _mongoOperations.insert(user);
            return true;
        }

        Update update = new Update().fromDocument(user.getDocument());
        _mongoOperations.update(AppUser.class).inCollection(_collectionName).matching(query).apply(update).all();
        return true;
    }

    private List<SimpleGrantedAuthority> getAuthority(String role) {
        return Arrays.asList(new SimpleGrantedAuthority("ROLE_" + role));
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser user = findWithUsername(username);

        if(user == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), getAuthority(user.getRole()));
    }
}