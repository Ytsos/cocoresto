package com.cocoresto.usermanagement.Model;

public class Token {
    private String _token;
    private String _username;

    public Token(){
    }

    public Token(String token, String username){
        this._token = token;
        this._username = username;
    }

    public Token(String token){
        this._token = token;
    }

    public String getToken() {
        return _token;
    }

    public void setToken(String token) {
        this._token = token;
    }

    public String getUsername() {
        return _username;
    }

    public void setUsername(String username) {
        this._username = username;
    }
}
