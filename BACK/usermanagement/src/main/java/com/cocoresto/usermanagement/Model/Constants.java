package com.cocoresto.usermanagement.Model;

public class Constants {
    public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 5*60*60;
    public static final String SIGNING_KEY = "qsdqsdqsdqsdqsdqsdqsdqsdqsdqd";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String ISSUER_KEY = "http://localhost/";
}