package com.cocoresto.usermanagement.Controller;

import com.cocoresto.usermanagement.Model.*;
import com.cocoresto.usermanagement.Service.MongoDbConnection;
import com.cocoresto.usermanagement.Utils.JwtTokenUtil;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;
import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class UserController {

    private MongoDbConnection _mongoDb;

    UserController(){
        this._mongoDb = new MongoDbConnection("users");
    }

    @Autowired
    private AuthenticationManager _authenticationManager;

    @Autowired
    private JwtTokenUtil _jwtTokenUtil;

    @Autowired
    private BCryptPasswordEncoder bcryptEncoder;


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ApiResponse<Token> login(@Valid @RequestBody LoginUser loginUser) throws AuthenticationException {
        _authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginUser.getUsername(), loginUser.getPassword()));

        final AppUser user = this._mongoDb.findWithUsername(loginUser.getUsername());
        if (user == null)
            return new ApiResponse<>(401, "failed login", new Token());
        final String token = _jwtTokenUtil.generateToken(user);
        return new ApiResponse<>(200, "success", new Token(token, user.getUsername()));
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ApiResponse<AppUser> register(@Valid @RequestBody RegisterUser newUser){
        final AppUser findUser = this._mongoDb.findWithUsername(newUser.username);

        AppUser user = new AppUser(new ObjectId(), newUser.username, bcryptEncoder.encode(newUser.password),
                "NORMAL", newUser.zipCode,
                newUser.firstname, newUser.lastname,
                newUser.birthDay, newUser.birthMonth, newUser.birthYear);

        if (findUser != null)
            return new ApiResponse<>(404, "User exist.", false);
        return new ApiResponse<>(HttpStatus.OK.value(), "User saved successfully.", this._mongoDb.saveOrUpdate(user));
    }

    @GetMapping("/test")
    public String ok(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "ok usermanagement";
    }

//    @DeleteMapping("/{id}")
//    public ApiResponse<Void> delete(@PathVariable int id) {
//        userService.delete(id);
//        return new ApiResponse<>(HttpStatus.OK.value(), "User deleted successfully.", null);
//    }
}
