package com.cocoresto.zuul.Configuration;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

public class ZuulAllFilter extends ZuulFilter {
    public static final String HEADER_AUTHORIZATION = "Host";

    public String filterType() {
        return "pre";
    }

    public int filterOrder() {
        return 0;
    }

    public boolean shouldFilter() {
        RequestContext ctx = RequestContext.getCurrentContext();
        return ctx.getRequest().getHeaders(HEADER_AUTHORIZATION).hasMoreElements();
    }

    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        String host = ctx.getRequest().getHeaders(HEADER_AUTHORIZATION).nextElement();
        ctx.getZuulRequestHeaders().put(HEADER_AUTHORIZATION, host);

        return null;
    }
}
